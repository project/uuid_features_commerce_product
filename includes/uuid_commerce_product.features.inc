<?php
/**
 * @file
 * Features hooks for the uuid_node features component.
 */

/**
 * Implements hook_features_export_options().
 */
function uuid_commerce_product_features_export_options() {
  $options = array();
  // Check what content types are enabled for uuid features export.
  $types = variable_get('uuid_features_commerce_product_entity_commerce_product', FALSE);
  if (!empty($types)) {
    $query = db_select('commerce_product', 'cp');
    $query->fields('cp', array('product_id', 'sku', 'title', 'type', 'uuid'))
      ->condition('type', $types)
      ->orderBy('type')
      ->orderBy('title');
    $products = $query->execute()->fetchAll();
    foreach ($products as $product) {
      $options[$product->uuid] = t('@type: @title', array(
        '@type' => $types[$product->type],
        '@title' => $product->title,
      ));
    }
  }
  drupal_alter('uuid_commerce_product_features_export_options', $options);
  return $options;
}

/**
 * Implements hook_features_export().
 */
function uuid_commerce_product_features_export($data, &$export, $module_name = '') {
  $pipe = array();

  $export['dependencies']['uuid_features_commerce_product'] = 'uuid_features_commerce_product';
  $export['dependencies']['commerce_product'] = 'commerce_product';

  uuid_features_commerce_product_load_module_includes();

  $product_ids = entity_get_id_by_uuid('commerce_product', $data);
  foreach ($product_ids as $uuid => $pid) {
    // Load the existing node, with a fresh cache.
    $product = commerce_product_load($pid, NULL, TRUE);
    $uuid = db_query("SELECT uuid FROM {commerce_product} WHERE product_id = $product->product_id")->fetchField();
    $product->uuid = $uuid;

    //TODO:For some reason, when the product is loaded, it isn't grabbing
    //the UUID or VUUID so we grab them manually. Have to write the logic
    //for reverting revisions as they go in their own table and if attached
    //to the product, features shows as a difference since the field gets dropped
    //on importing to commerce_product table.
    //$vuuid = db_query("SELECT uuid FROM {commerce_product_revision} WHERE product_id = $product->product_id")->fetchField();
    //$product->uuid = $uuid;

    //This doesn't work and features immediately flags it as a change
    //$product->vuuid = $vuuid;

    $export['features']['uuid_commerce_product'][$uuid] = $uuid;
    $pipe['commerce_product'][$product->type] = $product->type;

    // drupal_alter() normally supports just one byref parameter. Using
    // the __drupal_alter_by_ref key, we can store any additional parameters
    // that need to be altered, and they'll be split out into additional params
    // for the hook_*_alter() implementations.  The hook_alter signature is
    // hook_uuid_node_features_export_alter(&$export, &$pipe, $node)
    $data = &$export;
    $data['__drupal_alter_by_ref'] = array(&$pipe);
    drupal_alter('uuid_commerce_product_features_export', $data, $node);
  }

  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function uuid_commerce_product_features_export_render($module, $data) {
  $translatables = $code = array();

  uuid_features_commerce_product_load_module_includes();

  $code[] = '  $products = array();';
  $code[] = '';
  $product_ids = entity_get_id_by_uuid('commerce_product', $data);
  foreach ($product_ids as $uuid => $pid) {
    // Only export the node if it exists.
    if ($pid === FALSE) {
      continue;
    }
    // Attempt to load the node, using a fresh cache.
    $product = commerce_product_load($pid, NULL, TRUE);
    if (empty($product)) {
      continue;
    }
    if (!empty($product->path)) {
      $product->pathauto_perform_alias = FALSE;
    }
    $export = clone $product;

    // Use date instead of created, in the same format used by node_object_prepare.
    $export->date = format_date($export->created, 'custom', 'Y-m-d H:i:s O');

    // Don't cause conflicts with product_id/revision_id/time related fields.
    unset($export->product_id);
    unset($export->revision_id);
    unset($export->revision_timestamp);
    unset($export->changed);
    unset($export->created);
    unset($export->date);
    uuid_features_commerce_product_file_field_export($export, 'commerce_product');

    // Export taxonomy_term_reference with its UUID.
    $fields_info = field_info_instances('commerce_product', $product->type);
    foreach ($fields_info as $field_name => $value) {
      $field_info = field_info_field($field_name);
      $type = $field_info['type'];
      if ($type == 'taxonomy_term_reference') {
        if (!empty($product->$field_name)) {
          foreach ($product->{$field_name} as $lang => $terms) {
            foreach ($terms as $k => $v) {
              $tid = $product->{$field_name}[$lang][$k]['tid'];
              $term_uuids = entity_get_uuid_by_id('taxonomy_term', array($tid));
              $export->{$field_name}[$lang][$k]['uuid'] = $term_uuids[$tid];
              unset($export->{$field_name}[$lang][$k]['tid']);
            }
          }
        }
      }
    }

    // The hook_alter signature is:
    // hook_uuid_node_features_export_render_alter(&$export, $node, $module);
    drupal_alter('uuid_commerce_product_features_export_render', $export, $node, $module);

    $code[] = '  $products[] = ' . features_var_export($export) . ';';
  }

  if (!empty($translatables)) {
    $code[] = features_translatables_export($translatables, '  ');
  }
  $code[] = '  return $products;';
  $code = implode("\n", $code);
  return array('uuid_features_commerce_product_default_content' => $code);
}

/**
 * Implements hook_features_revert().
 */
function uuid_commerce_product_features_revert($module) {
  uuid_commerce_product_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 * Rebuilds products based on UUID from code defaults.
 */
function uuid_commerce_product_features_rebuild($module) {
  // Import the terms first.
  uuid_term_features_rebuild($module);

  $products = module_invoke($module, 'uuid_features_commerce_product_default_content');

  if (!empty($products)) {
    foreach ($products as $data) {
      $product = (object) $data;

      // Find the matching UUID, with a fresh cache.
      $pids = entity_get_id_by_uuid('commerce_product', array($product->uuid));
      if (isset($pids[$product->uuid])) {
          $pid = array_key_exists($product->uuid, $pids) ? $pids[$product->uuid] : FALSE;
          $existing = commerce_product_load($pid, NULL, TRUE);
          if (!empty($existing)) {
            $product->product_id = $existing->product_id;
            $product->revision_id = $existing->revision_id;
            $product->created = $existing->created;
          }
      }

      // Rebuild taxonomy_term_reference with its UUID.
      $fields_info = field_info_instances('commerce_product', $product->type);
      foreach ($fields_info as $field_name => $value) {
        $field_info = field_info_field($field_name);
        $type = $field_info['type'];
        if ($type == 'taxonomy_term_reference') {
          if (!empty($product->$field_name)) {
            foreach ($product->{$field_name} as $lang => $terms) {
              foreach ($terms as $k => $v) {
                $term_uuid = $product->{$field_name}[$lang][$k]['uuid'];
                $tids = entity_get_id_by_uuid('taxonomy_term', array($term_uuid));
                $product->{$field_name}[$lang][$k]['tid'] = $tids[$term_uuid];
                unset($product->{$field_name}[$lang][$k]['uuid']);
              }
            }
          }
        }
      }

      // The hook_alter signature is:
      // hook_uuid_node_features_rebuild_alter(&node, $module);
      drupal_alter('uuid_commerce_product_features_rebuild', $product, $module);
      entity_get_controller('commerce_product')->save($product);
    }
  }
}
